'use strict';
var calendarEvents = (function() {

    var calendarEvents = [];
    var currentEvent;

    var observerable = {
        listeners : {},
        addListener : function(object, evt, callback) {
            if ( !this.listeners.hasOwnProperty(evt) ) {
                this.listeners[evt] = [];
            }
            this.listeners[evt].push(object[callback]);
        },
        removeListener : function(object, evt, callback) {
            if ( this.listeners.hasOwnProperty(evt) ) {
                var i,length;
                for (i = 0, length = this.listeners[evt].length; i < length; i += 1) {
                    if( this.listeners[evt][i] === object[callback]) {
                        this.listeners[evt].splice(i, 1);
                    }
                }
            }
        },
        removeEventListener : function(evt) {
            if ( this.listeners.hasOwnProperty(evt) ) {
                delete this.listeners[evt];
            }
        },
        renameListener : function(evt, newEvt) {
            if ( this.listeners.hasOwnProperty(evt) ) {
                var listener = this.listeners[evt];
                delete this.listeners[evt];
                this.listeners[newEvt] = listener;
            }
        },
        triggerEvent : function(evt, args) {
            if ( this.listeners.hasOwnProperty(evt) )    {
                var i,length;
                for (i = 0, length = this.listeners[evt].length; i < length; i += 1) {
                    this.listeners[evt][i](args);
                }
            }
        },
        getListeners: function () {
            return this.listeners;
        }
    };

    function findIndexEventById (value) {
        for (var i = 0; i < calendarEvents.length; i++) {
            if (calendarEvents[i].id === value) {
                return i;
            }
        }
        return -1;
    }
    function findObjectEventById (value) {
        for (var i = 0; i < calendarEvents.length; i++) {
            if (calendarEvents[i].id === value) {
                return calendarEvents[i];
            }
        }
        return -1;
    }
    function findObjectEventByName (value) {
        for (var i = 0; i < calendarEvents.length; i++) {
            if (calendarEvents[i].nameEvent === value) {
                return calendarEvents[i];
            }
        }
        return -1;
    }
    function deleteEventById (id) {
        var indexObjectEvent = findIndexEventById(id);
        return calendarEvents.splice(indexObjectEvent, 1);
    }
    function getCollectionEvents(){
        return calendarEvents;
    }
    function saveEvent(event){
        calendarEvents.push(event);
        return event;
    }

    function findFirstStatusEventTrue(collectionEvents){
        var firstElement;
        for (var i = 0; i < collectionEvents.length; i++) {
            if (collectionEvents[i].statusEvent === true) {
                firstElement = collectionEvents[i];
                console.warn('timerModule: findFirstStatusEventTrue:');
                console.log(firstElement);
                return firstElement;
            }
        }
        return -1;
    }
    function searchUpcomingEvent (){
        var collectionEvents = getCollectionEvents();
        var upcomingEvent = findFirstStatusEventTrue(collectionEvents);
        if(upcomingEvent !== -1){
            for (var i = 0; i < collectionEvents.length; i++) {
                if ((upcomingEvent.dateEvent.getTime() >= collectionEvents[i].dateEvent.getTime()) && collectionEvents[i].statusEvent === true) {
                    upcomingEvent = collectionEvents[i];
                }
            }
        }
        console.warn('timerModule: upcomingEvent: ');
        console.log(upcomingEvent);
        return upcomingEvent;
    }

    function setEventDate (date){
        var newTime = Date.parse(date+'+03:00');
        var nowDate = new Date();
        if (isNaN(newTime)) {
            console.error('timerModule: Date invalid!');
            return -1;
        }
        var newDate = new Date(newTime);
        if (newDate.getYear() < nowDate.getYear() &&
            newDate.getMonth() < nowDate.getMonth() &&
            newDate.getDate() < nowDate.getDate() &&
            newDate.getMinutes() < nowDate.getMinutes()) {
            console.warn('timerModule: You entered a past date!');
            return -1;
        } else {
            return newDate;
        }
    }
    function generateId (){
        return new Date().getTime();
    }

    function checkStatusEvent (dateEvent){
        var nowDate = new Date();
        return !(dateEvent.getYear() < nowDate.getYear() &&
        dateEvent.getMonth() < nowDate.getMonth() &&
        dateEvent.getDate() < nowDate.getDate() &&
        dateEvent.getMinutes() < nowDate.getMinutes());
    }

    function getListTodayEventDate(){
        var collectionEvents = getCollectionEvents();
        var nowDate= new Date().getDate();
        var nowMonth = new Date().getMonth();
        var nowYear = new Date().getFullYear();
        var listEvents = [];

        for (var i = 0; i < collectionEvents.length; i++) {
            var day = collectionEvents[i].dateEvent.getDate();
            var month = collectionEvents[i].dateEvent.getMonth();
            var year = collectionEvents[i].dateEvent.getFullYear();
            if ((nowDate === day)&&(nowMonth === month)&&(nowYear === year)) {
                listEvents.push(collectionEvents[i]);
            }
        }
        return listEvents;
    }
    function getListMonthEventDate(){
        var collectionEvents = getCollectionEvents();
        var nowMonth = new Date().getMonth();
        var nowYear = new Date().getFullYear();
        var listEvents = [];

        for (var i = 0; i < collectionEvents.length; i++) {
            var month = collectionEvents[i].dateEvent.getMonth();
            var year = collectionEvents[i].dateEvent.getFullYear();
            if ((nowMonth === month)&&(nowYear === year)) {
                listEvents.push(collectionEvents[i]);
            }
        }
        return listEvents;
    }
    function getListWeekEventDate(){
        var collectionEvents = getCollectionEvents();
        var nowDate = new Date().getDate();
        var nowDay = new Date().getDay();
        var firstDay = nowDate - (nowDay-1);
        var lastDate = nowDate + (7 - nowDay);
        var nowMonth = new Date().getMonth();
        var nowYear = new Date().getFullYear();
        var listEvents = [];

        for (var i = 0; i < collectionEvents.length; i++) {
            var day = collectionEvents[i].dateEvent.getDate();
            var month = collectionEvents[i].dateEvent.getMonth();
            var year = collectionEvents[i].dateEvent.getFullYear();
            if (((day>=firstDay)&&(day <= lastDate))&&(nowMonth === month)&&(nowYear === year)) {
                listEvents.push(collectionEvents[i]);
            }
        }
        return listEvents;
    }
    function getListIntervalEventDate(date){
        var collectionEvents = getCollectionEvents();
        var first = setEventDate(date.first);
        var last = setEventDate(date.last);
        var listEvents = [];

        for (var i = 0; i < collectionEvents.length; i++) {
            var dateTime = collectionEvents[i].dateEvent;
            if ((first <= dateTime) && (last >= dateTime)) {
                listEvents.push(collectionEvents[i]);
            }
        }
        return listEvents;
    }

    function renameEvent(newName, objectEvent){
        observerable.renameListener(objectEvent.nameEvent, newName);
        objectEvent.nameEvent = newName;
        return objectEvent;
    }
    function changeDateEvent(dateEvent, objectEvent){
        objectEvent.dateEvent = setEventDate(dateEvent);
        objectEvent.statusEvent = true;
        observerable.triggerEvent('MAIN_EVENT_CHANGE_DATE', { event: objectEvent });
        startTimer();

        return objectEvent;
    }

    function clearEventTimeout(id){
        clearTimeout(id);
    }

    function leftTimeBeforeUpcomingEvent(upcomingEvent){
        var now = Date.now();
        var nexDate = upcomingEvent.dateEvent;
        var remaining = +nexDate - now;
        var seconds = remaining/1000;
        var minutes = Math.floor(seconds/60);
        if(minutes > 0){
            seconds = (remaining/1000-(60 * minutes));
        }
        console.warn('timerModule: leftTimeBeforeUpcomingEvent: ' + minutes + '(m)' + ':' + seconds + '(s)');
    }

    function checkCurrentEventAndStartTimer(newEvent){
        if (currentEvent) {
            if ((newEvent.dateEvent.getTime() < currentEvent.dateEvent.getTime())&&(newEvent.statusEvent === true)) {
                startTimer();
            } else {
                console.warn('timerModule: Current events upcoming!');
            }
        } else {
            startTimer();
        }
    }

    function defaultAlertEvent(upcomingEvent){
        console.info('timerModule: Only that the event occurred - ' + upcomingEvent.nameEvent + ' ' + upcomingEvent.dateEvent);
        upcomingEvent.statusEvent = false;

        observerable.triggerEvent(upcomingEvent.nameEvent, { event: upcomingEvent });
        observerable.triggerEvent('MAIN_EVENT_TRIGGER', { event: upcomingEvent });

        startTimer();
    }

    function startTimer (){
        if(currentEvent){
            clearEventTimeout(currentEvent.timerId);
            currentEvent = null;
        }
        var upcomingEvent = searchUpcomingEvent();

        if (upcomingEvent === -1) {
            console.info('timerModule: Upcoming events does not exist!');
            return -1;
        }
        currentEvent = upcomingEvent;
        var timeBeforeEvent = upcomingEvent.dateEvent.getTime() - Date.now();
        upcomingEvent.timerId = setTimeout(defaultAlertEvent, timeBeforeEvent, upcomingEvent);
        leftTimeBeforeUpcomingEvent(upcomingEvent);
    }

    function CalendarEvent (nameEvent, date){
        this.id = generateId();
        this.nameEvent = nameEvent;
        this.dateEvent = date;
        this.statusEvent = checkStatusEvent(date);
    }

    //public function
    function addNewEvent(nameEvent, dateEvent) {
        if((!nameEvent && typeof nameEvent !== 'string')||(!dateEvent && typeof nameEvent !== 'string')){
            console.error('timerModule: You entered the incorrect data, enter example: "name", "2016-02-01T15:53:00.000"');
            return -1;
        }
        if(findObjectEventByName(nameEvent) !== -1){
            console.warn('timerModule: The event with the same name already exists!');
            return -1;
        }
        var dateNewEvent = setEventDate(dateEvent);
        if(dateNewEvent === -1){
            return -1;
        }
        var newEvent = new CalendarEvent(nameEvent, dateNewEvent);

        console.warn('timerModule: newEvent:');
        console.log(newEvent);

        saveEvent(newEvent);
        checkCurrentEventAndStartTimer(newEvent);

        observerable.triggerEvent('MAIN_EVENT_ADD', { event: newEvent });

        return newEvent;
    }
    function addFuncForEvent (nameEvent, obj, func) {
        if(!nameEvent && !obj && !func){
            console.error('timerModule: You entered the incorrect data, enter example: "name",calendarEvents,"getAllEvents"');
            return -1;
        }
        observerable.addListener( obj, nameEvent, func);
        console.warn('timerModule: AddFuncForEvent succes! NameEvent: ' + nameEvent + ' Object and function: ' + obj + '.' + func + '()!');
    }
    function getAllEvents(){
        console.warn('timerModule: getAllEvents:');
        return getCollectionEvents();
    }
    function deleteEvent(id) {
        var objectEvent = findObjectEventById(id);
        if (objectEvent === -1) {
            console.error('timerModule: An event of this id does not exist!');
            return -1;
        }
        clearEventTimeout(objectEvent.timerId);
        observerable.removeEventListener(objectEvent.nameEvent);

        var deletedEvent = deleteEventById(objectEvent.id);
        console.warn('timerModule: Deleted success:');

        if (deletedEvent[0].id === currentEvent.id) {
            startTimer();
        }
        observerable.triggerEvent('MAIN_EVENT_DELETED', { event: deletedEvent[0] });

        return deletedEvent[0];
    }
    function changeEvent (id, newName, dateEvent) {
        var objectEvent = findObjectEventById(id);
        if (objectEvent === -1) {
            console.error('timerModule: An event of this id does not exist');
            return -1;
        }
        if (newName && (!dateEvent)) {
            if(findObjectEventByName(newName) !== -1){
                console.warn('timerModule: This event name already exists: ');
                return -1;
            }
            renameEvent(newName, objectEvent);
            console.warn('timerModule: Renamed: ');
            return objectEvent;
        }
        if(setEventDate(dateEvent) === -1) {
            return -1;
        }
        if (!newName && dateEvent) {
            changeDateEvent(dateEvent, objectEvent);
            console.warn('timerModule: Changed date: ')
            return objectEvent;
        }
        if (newName && dateEvent) {
            if(findObjectEventByName(newName) !== -1){
                console.warn('timerModule: This event name already exists: ');
                return -1;
            }
            renameEvent(newName, objectEvent);
            changeDateEvent(dateEvent, objectEvent);
            console.warn('timerModule: Renamed and changed the name of the event date: ');
            return objectEvent;
        }else{
            console.error('timerModule: Incorrect data');
            return -1;
        }
    }
    function getListEvents (interval) {
        var listEvents;
        if(typeof interval === 'string') {
            if (interval.toUpperCase() === 'DAY') {
                console.warn('timerModule: Events for the current day:');
                listEvents = getListTodayEventDate();
                return listEvents;
            }
            if (interval.toUpperCase() === 'WEEK') {
                console.warn('timerModule: Events for the current week:');
                listEvents = getListWeekEventDate();
                return listEvents;
            }
            if (interval.toUpperCase() === 'MONTH') {
                console.warn('timerModule: Events for the current month:');
                listEvents = getListMonthEventDate();
                return listEvents;
            }
        }
        if(typeof interval === 'object'){
            if(!interval.first && !interval.last){
                console.error('timerModule: Entered incorrect interval, exapmle: interval({first:date, last: date})');
                return -1;
            }
            console.warn('timerModule: Events for the current interval: ' + interval.first + '-' + interval.last);
            listEvents = getListIntervalEventDate(interval);
            return listEvents;
        }else{
            console.error('timerModule: Entered incorrect interval, exapmle: day, week, month or interval({first:date, last: date})');
            return -1;
        }
    }
    function getEventById (id) {
        var result = findObjectEventById(id);
        if(result === -1){
            console.error('timerModule: An event of this id does not exist!');
            return -1;
        }
        console.warn('timerModule: Find event by id success!');
        return result;
    }

    return {
        'addNewEvent': addNewEvent,
        'addFuncForEvent' : addFuncForEvent,
        'getAllEvents' : getAllEvents,
        'deleteEvent': deleteEvent,
        'changeEvent': changeEvent,
        'getListEvents': getListEvents,
        'getEventById': getEventById,
        'generateId' : generateId
    };

}());