'use strict';
var calendarEvents = (function (calendarEvents) {

    function containInArray(value, array) {
        for(var i = 0; i < array.length; i++) {
            if(Number(array[i]) === value){
                return true;
            }
        }
        return false;
    }

    function getMinValue(array){
        var min = array[0];
        for (var i = 0; i < array.length; i++) {
            if (min > array[i]){
                min = array[i];
            }
        }
        return min;
    }
    function getClosestRight(days){
        var now = new Date();
        var dayNow = now.getDay();
        var closestRight;
        for (var i = 0; i < days.length; i++) {
            var current = days[i];
            if (current > dayNow && (!closestRight || closestRight > current)) {
                closestRight = current;
            }
        }
        return closestRight;
    }
    function findNextDayEvent(days){
        var nextDateEvent;
        var now = new Date();
        var dayNow = now.getDay();
        var dateNow = now.getDate();
        var closestRight = getClosestRight(days);
        if (closestRight) {
            nextDateEvent = ((7 - Number(dayNow))-(7 - Number(closestRight))) + Number(dateNow);
        } else {
            closestRight = getMinValue(days);
            nextDateEvent = ( 7 - Number(dayNow)) + Number(dateNow) + Number(closestRight);
        }
        return nextDateEvent;
    }

    function getNextDayEventEveryday(dateTime){
        return dateTime.getDate()+1;
    }
    function getNextDayEventSameDays(daysRepeat){
        var days = daysRepeat.split(',');
        return findNextDayEvent(days);
    }

    function checkValidDaysEvent(days){
        if(days && typeof days === 'string'){
            if(days === 'all'){
                return 1;
            }else{
                var daysArr = days.split(',');
                for (var i = 0; i < daysArr.length; i++) {
                    if ((daysArr[i] > 7 || daysArr[i] < 1)|| isNaN(daysArr[i])){
                        return -1;
                    }
                }
                return 1;
            }
        }else{
            return -1;
        }
    }
    function checkValidTimeEvent(time){
        if(time && typeof time === 'string') {
            var timeHM = time.split(':');
            if (isNaN(timeHM[0]) || isNaN(timeHM[1])) {
                return -1;
            } else {
                return 1;
            }
        }else{
            return -1;
        }
    }

    function getNextDateEvent(time, days){
        var nextDay;
        var dayNow = new Date().getDay();
        var daysArray = days.split(',');
        var dateTime = new Date();
        var timeHM = time.split(':');

        dateTime.setHours(timeHM[0],timeHM[1], 0, 0);

        if(days === 'all'){
            if(dateTime.getTime() < Date.now()){
                nextDay = getNextDayEventEveryday(dateTime);
                dateTime.setDate(nextDay);
                return dateTime;
            }else{
                return dateTime;
            }
        }else{
            if((dateTime.getTime() > Date.now())&& containInArray(dayNow, daysArray)){
                return dateTime;
            }else{
                nextDay = getNextDayEventSameDays(days);
                dateTime.setDate(nextDay);
                return dateTime;
            }

        }
    }
    function setNextDateEvent(event){
        var nextDay;
        if(event.repeatEvent && event.daysRepeat){
            if(event.daysRepeat === 'all'){
                nextDay = getNextDayEventEveryday(event.dateEvent);
                event.dateEvent.setDate(nextDay);
                event.statusEvent = true;
                return event;
            }else{
                nextDay = getNextDayEventSameDays(event.daysRepeat);
                event.dateEvent.setDate(nextDay);
                event.statusEvent = true;
                return event;
            }
        }else{
            return -1;
        }
    }

    var object = {
        setNextDateForRepeatEvent: function (e) {
            var resultSetNextDateEvent = setNextDateEvent(e.event);
            if(resultSetNextDateEvent === -1){
                console.error('subRepeatEventModule: This event not repreat!');
                return -1;
            }
            console.warn('subRepeatEventModule: SetNextDateEvent for repeat event success!');
            return resultSetNextDateEvent;
        }
    };

    //public function
    calendarEvents.addRepeatEvent = function (nameEvent, time, days) {
        if(!nameEvent && !time && !days){
            console.error('subRepeatEventModule: You entered the incorrect data, example: "name", "17:16", "1,3"/"all"');
            return -1;
        }

        var checkValidDays = checkValidDaysEvent(days);
        if(checkValidDays === -1){
            console.erorr('subRepeatEventModule: You entered the incorrect days, enter example 1 to 7, days("all") or days("1,3")');
            return -1;
        }

        var checkValidTime = checkValidTimeEvent(time);
        if(checkValidTime === -1){
            console.erorr('subRepeatEventModule: You entered the incorrect time, enter example time("11:10")');
            return -1;
        }

        var dateEvent = getNextDateEvent(time, days);

        var newEvent = calendarEvents.addNewEvent(nameEvent, dateEvent);
        if(newEvent === -1){
            return -1;
        }

        newEvent.repeatEvent = true;
        newEvent.daysRepeat = days;

        calendarEvents.addFuncForEvent(newEvent.nameEvent, object, 'setNextDateForRepeatEvent');

        console.warn('subRepeatEventModule: Repeat event add success!');

        return newEvent;
    };
    calendarEvents.getNextDateEvent = function (time, days) {
        return getNextDateEvent(time, days);
    };

    return calendarEvents;

})(calendarEvents || {});