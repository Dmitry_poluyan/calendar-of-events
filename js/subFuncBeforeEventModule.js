'use strict';
var calendarEvents = (function ( ) {

    var globalFuncEvent;

    function getDateTimeBeforeEvent(dateEvent, time){
        var funcEventDate = new Date(dateEvent);
        var minutesEventFunc = funcEventDate.getMinutes() - time;
        funcEventDate.setMinutes(minutesEventFunc);
        return funcEventDate;
    }
    function clearEventTimeout(id){
        clearTimeout(id);
    }
    function changeFuncEvent(mainEvent){
        var changedFuncEvent;
        var funcEvent = calendarEvents.getEventById(mainEvent.funcEventId);
        if(funcEvent === -1){
            return -1;
        }
        var funcEventDate = getDateTimeBeforeEvent(mainEvent.dateEvent, funcEvent.timeBeforeMainEvent);

        if(mainEvent.repeatEvent === true){
            var timeDate = funcEventDate.getHours() + ':' + funcEventDate.getMinutes();
            funcEventDate = calendarEvents.getNextDateEvent(timeDate, mainEvent.daysRepeat);

            changedFuncEvent = calendarEvents.changeEvent(funcEvent.id, null, funcEventDate);
            if(changedFuncEvent === -1){
                return -1;
            }

            console.warn('subFuncBeforeEventModule: FuncEvent changed success!');
            return changedFuncEvent;
        }else{
            if(funcEventDate.getTime() < Date.now() ){
                var resultDeleteFuncEvent = calendarEvents.deleteEvent(funcEvent.id);
                if(resultDeleteFuncEvent === -1){
                    return -1;
                }
                console.warn('subFuncBeforeEventModule: These date and time is in the past! FuncEvent deleted!');
                return resultDeleteFuncEvent;
            }else{
                changedFuncEvent = calendarEvents.changeEvent(funcEvent.id, null, funcEventDate);
                if(changedFuncEvent === -1){
                    return -1;
                }

                console.warn('subFuncBeforeEventModule: FuncEvent changed success!');
                return changedFuncEvent;
            }
        }
    }

    function findFirstStatusEventTrueAndGlobalStatusFalse(collectionEvents){
        var firstElement;
        for (var i = 0; i < collectionEvents.length; i++) {
            if ((collectionEvents[i].statusEvent === true && !collectionEvents[i].globalStatus)&&
                (!collectionEvents[i].globalStatus && !collectionEvents[i].funcEvent && !collectionEvents[i].globalFunc)) {
                firstElement = collectionEvents[i];
                console.warn('subFuncBeforeEventModule: findFirstStatusEventTrueAndGlobalStatusFalse:');
                return firstElement;
            }
        }
        return -1;
    }
    function searchUpcomingEvent (time){
        var collectionEvents = calendarEvents.getAllEvents();
        var upcomingEvent = findFirstStatusEventTrueAndGlobalStatusFalse(collectionEvents);
        if(upcomingEvent !== -1){
            for (var i = 0; i < collectionEvents.length; i++) {
                if ((upcomingEvent.dateEvent.getTime() >= collectionEvents[i].dateEvent.getTime()) &&
                    (collectionEvents[i].statusEvent === true &&
                    !collectionEvents[i].globalStatus &&
                    !collectionEvents[i].funcEvent &&
                    !collectionEvents[i].globalFunc)) {
                    upcomingEvent = collectionEvents[i];
                }
            }
        }
        if(upcomingEvent === -1){
            if(!globalFuncEvent){
                return -1;
            }
            globalFuncEvent.statusEvent = false;
            clearEventTimeout(globalFuncEvent.timerId);
            console.warn('subFuncBeforeEventModule: Can not upcoming events for global function!');
            return -1;
        }
        var globalFuncEventDate = getDateTimeBeforeEvent(upcomingEvent.dateEvent, time);
        var nowDate = new Date();

        if(globalFuncEventDate.getYear() < nowDate.getYear() &&
            globalFuncEventDate.getMonth() < nowDate.getMonth()&&
            globalFuncEventDate.getDate() < nowDate.getDate()&&
            globalFuncEventDate.getMinutes() < nowDate.getMinutes()){
            upcomingEvent.globalStatus = true;
            searchUpcomingEvent(time);
        }
        console.warn('subFuncBeforeEventModule: searchUpcomingEvent for global function: ');
        return upcomingEvent;
    }

    function nextEventForGlobalFunc(globalFuncEvent){
        var mainEvent = calendarEvents.getEventById(globalFuncEvent.mainEventId);
        if(mainEvent === -1){
            return -1;
        }
        mainEvent.globalStatus = true;

        return nextDateGlobalFunc(globalFuncEvent);
    }
    function nextDateGlobalFunc(globalFuncEvent){
        var upcomingEvent = searchUpcomingEvent(globalFuncEvent.timeBeforeEvent);
        if(upcomingEvent === -1){
            return -1;
        }

        globalFuncEvent.mainEventId = upcomingEvent.id;
        globalFuncEvent.statusEvent = true;

        var globalFuncEventNextDate = getDateTimeBeforeEvent(upcomingEvent.dateEvent, globalFuncEvent.timeBeforeEvent);
        var resultChangeEvent= calendarEvents.changeEvent(globalFuncEvent.id, null, globalFuncEventNextDate);
        if(resultChangeEvent === -1){
            return -1;
        }
        console.warn('subFuncBeforeEventModule: Set globalFuncEventNextDate!');
        return resultChangeEvent;
    }

    var object = {
        deleteFuncEvent: function (e) {
            var resultDeleteFuncEvent = calendarEvents.deleteEvent(e.event.id);
            if (resultDeleteFuncEvent === -1) {
                return -1;
            }
            console.warn('subFuncBeforeEventModule: FuncEvent deleted success!');
            return resultDeleteFuncEvent;
        },
        deleteIfFuncEvent: function (e) {
            var event = e.event;
            if(event.funcEvent){
                var mainEvent = calendarEvents.getEventById(event.mainEventId);
                if(mainEvent === -1){
                    return -1;
                }
                delete mainEvent.funcEventId;

                console.warn('subFuncBeforeEventModule: Property mainEvent.funcEventId deleted!');
                return mainEvent;
            }
            if(event.funcEventId){
                var resultDeleteEvent = calendarEvents.deleteEvent(event.funcEventId);
                if(resultDeleteEvent === -1){
                    return -1;
                }
                console.warn('subFuncBeforeEventModule: FuncEvent deleted!');
                return resultDeleteEvent;
            }
        },
        changeIfFuncEvent: function (e) {
            var event = e.event;
            if(event.funcEventId){
                var resultChangeFuncEvent = changeFuncEvent(event);
                if(resultChangeFuncEvent === -1){
                    return -1;
                }
                console.warn('subFuncBeforeEventModule: Func event changed success!');
                return resultChangeFuncEvent;
            }
        },
        nextEventForGlobalFunc: function (e) {
            var globalFuncEvent = e.event;
            var resultNextEventForGlobalFunc = nextEventForGlobalFunc(globalFuncEvent);
            if (resultNextEventForGlobalFunc === -1) {
                return -1;
            }
            return resultNextEventForGlobalFunc;
        },
        deleteGlobalStatusForRepeatEvent: function (e) {
            var event = e.event;
            if(event.repeatEvent && event.globalStatus){
                delete event.globalStatus;
                var resultNextDateGlobalFunc = nextDateGlobalFunc(globalFuncEvent);
                if (resultNextDateGlobalFunc === -1) {
                    return -1;
                }
                return resultNextDateGlobalFunc;
            }
        },
        startSearchNextEventForGlobalFuncIfAddNewEvent: function (e) {
            var event = e.event;
            if(globalFuncEvent && event.id !== globalFuncEvent.id){
                var resultNextDateGlobalFunc;
                var resultGetDateTimeBeforeEvent = getDateTimeBeforeEvent(event.dateEvent, globalFuncEvent.timeBeforeEvent);
                if(resultGetDateTimeBeforeEvent.getTime() < Date.now()){
                    event.globalStatus = true;
                    return -1;
                }else{
                    if(!globalFuncEvent.statusEvent){
                        resultNextDateGlobalFunc = nextDateGlobalFunc(globalFuncEvent);
                        if (resultNextDateGlobalFunc === -1) {
                            return -1;
                        }
                        return resultNextDateGlobalFunc;
                    }
                    if(resultGetDateTimeBeforeEvent.getTime() < globalFuncEvent.dateEvent.getTime()){
                        resultNextDateGlobalFunc = nextDateGlobalFunc(globalFuncEvent);
                        if (resultNextDateGlobalFunc === -1) {
                            return -1;
                        }
                        return resultNextDateGlobalFunc;
                    }
                    return -1;
                }
            }
        },
        startSearchNextEventForGlobalFuncIfDeleteCurrentEvent : function (e) {
            var event = e.event;
            if(globalFuncEvent && event.id !== globalFuncEvent.id){
                if(event.id === globalFuncEvent.mainEventId){
                    var resultNextDateGlobalFunc = nextDateGlobalFunc(globalFuncEvent);
                    if (resultNextDateGlobalFunc === -1) {
                        return -1;
                    }
                    return resultNextDateGlobalFunc;
                }
            }
        },
        startSearchNextEventForGlobalFuncIfChangeEvent: function (e) {
            var event = e.event;
            if(globalFuncEvent && event.id !== globalFuncEvent.id){
                var resultNextDateGlobalFunc;
                var resultGetDateTimeBeforeEvent = getDateTimeBeforeEvent(event.dateEvent, globalFuncEvent.timeBeforeEvent);
                if(resultGetDateTimeBeforeEvent.getTime() < Date.now()){
                    event.globalStatus = true;
                    return -1;
                }else{
                    if(!globalFuncEvent.statusEvent){
                        resultNextDateGlobalFunc = nextDateGlobalFunc(globalFuncEvent);
                        if (resultNextDateGlobalFunc === -1) {
                            return -1;
                        }
                        return resultNextDateGlobalFunc;
                    }
                    if((resultGetDateTimeBeforeEvent.getTime() < globalFuncEvent.dateEvent.getTime())||(event.id === globalFuncEvent.mainEventId)){
                        resultNextDateGlobalFunc = nextDateGlobalFunc(globalFuncEvent);
                        if (resultNextDateGlobalFunc === -1) {
                            return -1;
                        }
                        return resultNextDateGlobalFunc;
                    }
                    return -1;
                }
            }
        }
    };
    function subscribeForEvents(){
        calendarEvents.addFuncForEvent('MAIN_EVENT_TRIGGER', object, 'deleteGlobalStatusForRepeatEvent');

        calendarEvents.addFuncForEvent('MAIN_EVENT_ADD', object, 'startSearchNextEventForGlobalFuncIfAddNewEvent');
        calendarEvents.addFuncForEvent('MAIN_EVENT_DELETED', object, 'startSearchNextEventForGlobalFuncIfDeleteCurrentEvent');
        calendarEvents.addFuncForEvent('MAIN_EVENT_CHANGE_DATE', object, 'startSearchNextEventForGlobalFuncIfChangeEvent');

        calendarEvents.addFuncForEvent('MAIN_EVENT_DELETED', object, 'deleteIfFuncEvent');
        calendarEvents.addFuncForEvent('MAIN_EVENT_CHANGE_DATE', object, 'changeIfFuncEvent');
    }
    subscribeForEvents();

    //public function
    calendarEvents.addFuncBeforeEvent = function (id, time, obj, func) {
        if(!id && !time && !obj && !func){
            console.error('subRepeatEventModule: You entered the incorrect data, example: 10, calendarEvents, "getAllEvents"');
            return -1;
        }
        var mainEvent = calendarEvents.getEventById(id);
        if(mainEvent === -1){
            return -1;
        }
        if(mainEvent.funcEventId){
            console.error('subFuncBeforeEventModule: This event already have pre function!');
            return -1;
        }

        var newFuncEvent;
        var nameNewFuncEvent = calendarEvents.generateId() + '-FUNC';
        var funcEventDate = getDateTimeBeforeEvent(mainEvent.dateEvent, time);

        if(mainEvent.repeatEvent){
            var timeDate = funcEventDate.getHours() + ':' + funcEventDate.getMinutes();
            newFuncEvent = calendarEvents.addRepeatEvent(nameNewFuncEvent, timeDate, mainEvent.daysRepeat);
            if(newFuncEvent === -1){
                return -1;
            }

            newFuncEvent.mainEventId = mainEvent.id;
            newFuncEvent.funcEvent = true;
            newFuncEvent.timeBeforeMainEvent = time;

            mainEvent.funcEventId = newFuncEvent.id;

            calendarEvents.addFuncForEvent(newFuncEvent.nameEvent, obj, func);

            return newFuncEvent;
        }else{
            if(funcEventDate.getTime() < Date.now()){
                console.warn('subFuncBeforeEventModule: These date and time is in the past!');
                return -1;
            }else{
                newFuncEvent = calendarEvents.addNewEvent(nameNewFuncEvent, funcEventDate);
                if(newFuncEvent === -1){
                    return -1;
                }

                newFuncEvent.mainEventId = mainEvent.id;
                newFuncEvent.funcEvent = true;
                newFuncEvent.timeBeforeMainEvent = time;

                mainEvent.funcEventId = newFuncEvent.id;

                calendarEvents.addFuncForEvent(newFuncEvent.nameEvent, obj, func);
                calendarEvents.addFuncForEvent(newFuncEvent.nameEvent, object, 'deleteFuncEvent');

                console.warn('subFuncBeforeEventModule: Pre function for the event success added!');

                return newFuncEvent;
            }
        }


    };

    calendarEvents.addGlobalFunction = function (time, obj, func) {
        if(globalFuncEvent){
            console.warn('subFuncBeforeEventModule: Alredy have global func!');
            return -1;
        }
        var nameNewGlobalFuncEvent = calendarEvents.generateId() + '-GLOBAL';
        var upcomingEvent = searchUpcomingEvent(time);

        if(upcomingEvent === -1){
            console.warn('subFuncBeforeEventModule: newGlobalFuncEvent do not created, because have not upcoming event!');
            return -1;
        }
        var globalFuncEventDate = getDateTimeBeforeEvent(upcomingEvent.dateEvent, time);
        globalFuncEvent = calendarEvents.addNewEvent(nameNewGlobalFuncEvent, globalFuncEventDate);
        if(globalFuncEvent === -1){
            return -1;
        }

        globalFuncEvent.globalFunc = true;
        globalFuncEvent.mainEventId = upcomingEvent.id;
        globalFuncEvent.timeBeforeEvent = time;

        calendarEvents.addFuncForEvent(globalFuncEvent.nameEvent, obj, func);
        calendarEvents.addFuncForEvent(globalFuncEvent.nameEvent, object, 'nextEventForGlobalFunc');

        console.warn('subFuncBeforeEventModule: newGlobalFuncEvent created success!');

        return globalFuncEvent;
    };

    return calendarEvents;

})(calendarEvents || {});